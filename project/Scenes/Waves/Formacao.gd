extends Node2D

signal final_formacao

var enemy_count: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	for enemy in get_tree().get_nodes_in_group(Groups.ENEMY_GROUP):
		enemy.connect ("destroyed", self, "_on_child_destroyed")
		enemy_count += 1
	#for child in get_children():
	#	if not child is Area2D: continue
	#	child.connect ("destroyed", self, "_on_child_destroyed")
	#	enemy_count += 1

func _process(delta):
	if enemy_count == 0:
		emit_signal("final_formacao")
		queue_free()

func _on_child_destroyed():
	enemy_count -= 1
