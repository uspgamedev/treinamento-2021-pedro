extends Area2D

# Valores default
var speed: float = 550.0
var direction: Vector2 = Vector2.UP

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group(Groups.PLAYER_BULLET_GROUP)
	pass # Replace with function body.


func _process(delta):
	self.position += direction * speed * delta


func set_atributes(new_pos, new_speed = 550.0, new_dir = Vector2.UP):
	self.position = new_pos
	self.speed = new_speed
	self.direction = new_dir.normalized()
	self.rotation = Vector2.UP.angle_to(new_dir)

# Para o jogo não explodir a bullet é removida da cena principal se sair da tela
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
