extends Area2D

# Valores default
var speed: float = 550.0
var direction: Vector2 = Vector2.DOWN

func set_atributes(new_pos, new_dir = Vector2.DOWN, new_speed = 550.0):
	self.position = new_pos
	self.speed = new_speed
	self.direction = new_dir.normalized()
	self.rotation = Vector2.DOWN.angle_to(new_dir)

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group(Groups.ENEMY_BULLET_GROUP)
	pass # Replace with function body.

func _process(delta):
	self.position += direction * speed * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
