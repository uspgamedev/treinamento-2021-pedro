extends Area2D

var speed: float = 300.0
var mov_direction: Vector2 = Vector2.ZERO
var velocity: Vector2

var health: int = 4
signal player_damaged
signal player_dead

onready var rShooter = $RShooter
onready var lShooter = $LShooter
onready var audioPlayer = $AudioStreamPlayer
onready var audioMorte = $AudioMorte

var BULLET_SCENE: PackedScene = preload("res://Scenes/Bullets/BulletLaser.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group(Groups.PLAYER_GROUP)
	pass # Replace with function body.

func _physics_process(delta):
	if not is_visible():
		return
	mov_direction = Vector2.ZERO
	if Input.is_action_pressed("player_up"):
		mov_direction += Vector2(0, -1)
	if Input.is_action_pressed("player_down"):
		mov_direction += Vector2(0, 1)
	if Input.is_action_pressed("player_left"):
		mov_direction += Vector2(-1, 0)
	if Input.is_action_pressed("player_right"):
		mov_direction += Vector2(1, 0)
		
	if Input.is_action_just_pressed("player_shoot"):
		shoot()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.position += mov_direction.normalized() * speed * delta
	stay_in_viewport()

func shoot():
	var bullet1 = BULLET_SCENE.instance()
	var bullet2 = BULLET_SCENE.instance()
	
	audioPlayer.play()
	get_parent().add_child(bullet1)
	get_parent().add_child(bullet2)
	bullet1.set_atributes(rShooter.global_position)
	bullet2.set_atributes(lShooter.global_position)
		
func stay_in_viewport():
	var current_x: float = self.position.x
	var current_y: float = self.position.y
	
	self.position.x = clamp(current_x, 0, get_viewport_rect().size.x)
	self.position.y = clamp(current_y, 0, get_viewport_rect().size.y)
 

func damage():
	emit_signal("player_damaged")
	audioMorte.play()
	health -= 1
	if(health <= 0):
		hide()
		emit_signal("player_dead")

func _on_Player_area_entered(area):
	if(area.is_in_group(Groups.ENEMY_BULLET_GROUP)):
		area.queue_free()
		damage()
	elif(area.is_in_group(Groups.ENEMY_GROUP)):
		#if (!area.alive):
		#	return
		damage()
		area.damage(5)

