extends Node2D

const num_wave: int = 4
const GAME_OVER_SCENE: PackedScene = preload("res://Scenes/UI/GameOver/GameOver.tscn")

var FORMACOES: Array = [
	preload("res://Scenes/Waves/Formacao1.tscn"), 
	preload("res://Scenes/Waves/Formacao2.tscn"),
	preload("res://Scenes/Waves/Formacao3.tscn"),
	preload("res://Scenes/Waves/Formação4.tscn")
]

var waves_buffer: Array = [0,1,2,3]

var current_wave: int = 0
onready var player = get_tree().get_nodes_in_group(Groups.PLAYER_GROUP)[0]

func _ready():
	$BackgroundMusic.play()
	player.connect("player_dead", self, "_on_Player_dead")
	spawn(waves_buffer[current_wave])

func prepare_next_wave():
	current_wave = (current_wave+1) % num_wave
	if (current_wave == 0):
		randomize()
		waves_buffer.shuffle()
	yield(get_tree().create_timer(1.0), "timeout")
	spawn(waves_buffer[current_wave])
	
func spawn(wave):
	var formacao = FORMACOES[wave].instance()
	self.add_child(formacao)
	formacao.connect("final_formacao", self, "prepare_next_wave")

func _on_Player_dead():
	$Overlay/GameOver.visible = true
	get_tree().paused = true

func _on_BackgroundMusic_finished():
	$BackgroundMusic.play()
