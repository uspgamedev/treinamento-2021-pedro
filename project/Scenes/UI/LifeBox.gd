extends HBoxContainer
var LIFE_BOX: PackedScene = preload("res://Scenes/UI/LifeBox.tscn")

var player: Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_tree().get_nodes_in_group(Groups.PLAYER_GROUP)[0]
	for i in player.health:
		 add_child(LIFE_BOX.instance())
	player.connect("player_damaged", self, "_damage")


func _damage():
	if get_child_count() > 0:
		get_child(0).queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
