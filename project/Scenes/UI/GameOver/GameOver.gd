extends Control

const MENU = "res://Scenes/UI/MainMenu/Menu.tscn"
export(NodePath) onready var MENU_BUTTON = get_node(MENU_BUTTON) as Button
export(NodePath) onready var EXIT_BUTTON = get_node(EXIT_BUTTON) as Button

# Called when the node enters the scene tree for the first time.
func _ready():
	MENU_BUTTON.connect("pressed", self, "_on_Menu_pressed")
	EXIT_BUTTON.connect("pressed", self, "_on_Exit_pressed")
	pass


func _on_Menu_pressed():
	get_tree().paused = false
	get_tree().change_scene(MENU)

func _on_Exit_pressed():
	get_tree().quit()
