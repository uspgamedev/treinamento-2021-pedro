extends Control

const GAMEPLAY = "res://Scenes/MainGameplay/MainGameplay.tscn"
export(NodePath) onready var START_BUTTON = get_node(START_BUTTON) as Button
export(NodePath) onready var EXIT_BUTTON = get_node(EXIT_BUTTON) as Button

# Called when the node enters the scene tree for the first time.
func _ready():
	START_BUTTON.connect("pressed", self, "_on_Start_pressed")
	EXIT_BUTTON.connect("pressed", self, "_on_Exit_pressed")


func _on_Start_pressed():
	get_tree().change_scene(GAMEPLAY)

func _on_Exit_pressed():
	get_tree().quit()
