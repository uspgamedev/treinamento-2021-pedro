extends "res://Scenes/Enemies/Enemy.gd"

func _ready():
	self.add_to_group(Groups.ENEMY_GROUP)

func _on_EnemyBlack3_area_entered(area):
	if area.is_in_group(Groups.PLAYER_BULLET_GROUP) and health > 0:
		area.queue_free()
		damage(1)
