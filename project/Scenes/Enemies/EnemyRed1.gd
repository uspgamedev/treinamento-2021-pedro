extends "res://Scenes/Enemies/Enemy.gd"

func _ready():
	self.add_to_group(Groups.ENEMY_GROUP)

# Overwrite the function from super
func _shoot():
	if(!visible): 
		return
	var bullet1 = BULLET_SCENE.instance()
	get_tree().get_current_scene().add_child(bullet1)
	var dir = Groups.get_tree().get_nodes_in_group(Groups.PLAYER_GROUP)[0].global_position-self.global_position
	bullet1.set_atributes(self.global_position, dir.normalized())

func _on_EnemyRed_area_entered(area):
	if area.is_in_group(Groups.PLAYER_BULLET_GROUP) and health > 0:
		area.queue_free()
		damage(1)
