extends Area2D

# Enemy attributes
var health: int = 3

var shooting: bool = false
var shoot_cooldown: float = 1.5

var BULLET_SCENE: PackedScene = preload("res://Scenes/Bullets/BulletEnemy1.tscn")
var EXPLOSION_SCENE: PackedScene = preload("res://Scenes/Effects/EnemyExplosion.tscn")

signal destroyed

# Enemy functions
func start_shooting():
	shooting = true

func _shoot():
	if (!visible): 
		return
	var bullet = BULLET_SCENE.instance()
	get_tree().get_current_scene().add_child(bullet)
	bullet.set_atributes(self.global_position)

func destroy():
	var explosion = EXPLOSION_SCENE.instance()
	get_tree().get_current_scene().add_child(explosion)
	explosion.position = self.global_position
	
	emit_signal("destroyed")
	shooting = false
	hide()
	set_deferred("monitorable", false)
	set_deferred("monitoring", false)
	
func damage(damage: int):
	health -= damage
	if health <= 0:
		destroy()
