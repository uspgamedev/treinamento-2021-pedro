extends "res://Scenes/Enemies/Enemy.gd"

onready var shoot_cooldown_timer = $ShootCooldown

# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group(Groups.ENEMY_GROUP)


func _physics_process(_delta):
	if shooting and shoot_cooldown_timer.is_stopped():
		shoot_cooldown_timer.start(shoot_cooldown)
		_shoot()

func _on_EnemyBlack1_area_entered(area):
	if area.is_in_group(Groups.PLAYER_BULLET_GROUP) and health > 0:
		area.queue_free()
		damage(1)
